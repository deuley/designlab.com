# [1.11.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.3...v1.11.0) (2020-05-27)


### Features

* **report:** Add report object documentation ([7293914](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/7293914f01ae4db15d67a420d98d61bdab7e4fcb))

## [1.10.3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.2...v1.10.3) (2020-05-27)


### Bug Fixes

* **file-uploader:** Fix casing to ensure Todo banner is correctly parsed ([e9732b3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e9732b3386de7caeb8d001157c3031b921b6d7b6))

## [1.10.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.1...v1.10.2) (2020-05-25)


### Bug Fixes

* **file-uploader:** Remove broken anchors ([c430214](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/c430214435bbe60bab3579258fb3742202a51fbb))

## [1.10.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.0...v1.10.1) (2020-05-22)


### Bug Fixes

* **homepage:** Updated link to Figma UI Kit ([9d3eb22](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9d3eb224b39a9a0b1aa99282c989727509ea6f95))

# [1.10.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.9.0...v1.10.0) (2020-05-21)


### Features

* **tree:** Add design spec link for Tree component ([0e8def7](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/0e8def726a393f86224623c7b645340df7c99edd))

# [1.9.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.8.0...v1.9.0) (2020-05-20)


### Features

* **Sorting:** Refining sorting vs filtering guidelines ([53f1543](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/53f154311cdef36c08723d5eb40ce5a353e1830e))

# [1.8.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.7.0...v1.8.0) (2020-05-20)


### Features

* **progressbar:** Add documentation for progress bar ([cd2739b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/cd2739b651cffe2ca6e82ff9e7dafcdef21d2674))

# [1.7.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.6.0...v1.7.0) (2020-05-20)


### Features

* **banner:** Add dismissal guidelines ([1dd8338](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1dd8338148342f41ecd75fcb900f481e3fc373cf))

# [1.6.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.5.1...v1.6.0) (2020-05-20)


### Features

* **token:** Add Pajamas UI Kit link for tokens ([296ffa1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/296ffa175ca182855d08f8648b9f5f9bf4e63265))

## [1.5.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.5.0...v1.5.1) (2020-05-15)


### Bug Fixes

* **Pajamas UI Kit:** Remove beta reference to correct resource anchor ([da74802](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/da74802b144d5e3f97828ce8df5e1ed8001b2b72))

# [1.5.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.4.0...v1.5.0) (2020-05-14)


### Features

* **fileuploader:** Add documentation for file uploader ([9726a98](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9726a98dd79629b82c97984f686b50cb33e48193))

# [1.4.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.3.0...v1.4.0) (2020-05-14)


### Features

* **filter:** Add vue component link and component example ([450ac8e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/450ac8e311ed6198abc175f0ffab6c6770ea1697))

# [1.3.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.2.1...v1.3.0) (2020-05-14)


### Features

* **sorting:** Add Pajamas UI Kit link for sorting component ([b94ef04](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b94ef04af5f4e03ade50463c353d370a8eef6fbc))

## [1.2.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.2.0...v1.2.1) (2020-05-14)


### Bug Fixes

* **css:** Rename colour gray-0 to gray-10 ([f2376c8](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f2376c8375f6dee1e87e46eb02e1ca4b06cf97f3))

# [1.2.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.1.1...v1.2.0) (2020-05-13)


### Features

* **statustable:** Add new columns to component status table ([63dc07b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/63dc07b4d61268f1dc59157471a0bfeeff99daea))

## [1.1.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.1.0...v1.1.1) (2020-05-12)


### Bug Fixes

* Update design spec links for several components ([d7755a1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d7755a1920a65e703027c9236abe64899cbd3c4d))

# [1.1.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.0.0...v1.1.0) (2020-05-08)


### Features

* **buttons:** Add position exception for alerts ([f12bb3d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f12bb3d555b65c8fa565ffac109cc436ed032322))

# 1.0.0 (2020-04-29)


### Bug Fixes

* Support links to Markdown pages ([8a8e87b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/8a8e87bf303b481a2dc4eb25b79a3a258868a25f))
